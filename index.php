<?php
  ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require 'tmhOAuth/tmhOAuth.php';
$tmhOAuth = new tmhOAuth(array(
'consumer_key' => 'CONSUMERKEY',
'consumer_secret' => 'CONSUMERSECRET',
'user_token' => 'USERTOKEN',
'user_secret' => 'USERSECRET'
));
$blacklist = array("jonesy1551", "niallflorence", "contestgnome", "jooohnxbuch", "jasondh153", "jwatson50", "ikegameshop", "kshp_db", "hargowwong", "justwin4once", "whissocialmedia", "thegiveawaybot", "twittesty", "jennym1979", "footstar01", "ptn2pnh", "trapdrugs", "zorquaah", "_cathyj_", "BetterWayToPlay");
// Create connection
$conn = new mysqli("localhost", "root", "mysqlisawesome", "twitterbot");
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

?>

<h2>New Tweets</h2>

<?php

function cleanText($tweet){
  $tweet = str_replace("'", "''", $tweet);
  return $tweet;
}

//Strings to look for in tweets
function compileWinRT(){
  $q = "";
  $winArray = array("Retweet 2 win", "RT 2 win", "Retweet to win", "RT to win");
  foreach($winArray as $t){
    $q .= "\"" . $t . "\" OR ";
  }
	$q = substr($q, 0, -3);
	
	
  $q .= " OR (\"win\" AND (RT OR retweet OR re-tweet))";

  return $q;
}

function findContestTweets($q){
	global $tmhOAuth, $conn, $blacklist;
	
	$code = $tmhOAuth->request('GET', $tmhOAuth->url('1.1/search/tweets'), $q);
 
		if ($code == 200){
		$data = json_decode($tmhOAuth->response['response'], true);
		foreach($data['statuses'] as $s){
			$result = $conn->query("SELECT * FROM tweets WHERE tweetid = '" . $s['id'] . "' or content = '" . cleanText($s['text']) . "'");
			if ($result->num_rows == 0 && !in_array(strtolower($s['user']['screen_name']), $blacklist)) retweet($s);
		}
	}
}

function reTweet($s){
  global $tmhOAuth, $conn;
  echo("Retweet: " . $s['text'] . "<br><br>");
  $code = $tmhOAuth->user_request(array('method' => 'POST', 'url' => $tmhOAuth->url('1.1/statuses/retweet/' . $s['id']), 'params' => array(), 'multipart' => false));
  mysqli_query($conn, "INSERT INTO tweets VALUES('" . $s['id'] . "', '" . cleanText($s['text']) . "', '" . $s['created_at'] . "')") or die("INSERT INTO tweets VALUES(" . $s['id'] . ", '" . $s['text'] . "', '" . date( 'Y-m-d H:i:s', strtotime($s['created_at']) ) . "')");
  
  if(strpos(strtolower($s['text']), "follow") > 0) followUser($s['user']["id_str"]);
        
}

function deleteOldFollowers($count){
	global $tmhOAuth, $conn;
  $result = $conn->query("SELECT * FROM follows ORDER BY followid LIMIT " . $count);
       while($row = $result->fetch_assoc()){
            $code = $tmhOAuth->user_request(array('method' => 'POST', 'url' => $tmhOAuth->url('1.1/friendships/destroy'), 'params' => array('user_id' => $row['userid']), 'multipart' => false));
            if($code == 200) mysqli_query($conn, "DELETE FROM follows WHERE userid = '" . $row['userid'] . "'");
        }
}

function followUser($userid){
  global $tmhOAuth, $conn;
  if (mysqli_num_rows($conn->query("SELECT * FROM follows WHERE userid = '" . $userid . "'")) == 0){  //Are we currently following user
     $result = $conn->query("SELECT COUNT(*) as total FROM follows");
     $row = $result->fetch_assoc();
     if((int)$row['total'] > 1800){  //can only follow 2000 people max
       deleteOldFollowers(100);
     }
     
     $code = $tmhOAuth->user_request(array('method' => 'POST', 'url' => $tmhOAuth->url('1.1/friendships/create'), 'params' => array('user_id' => $userid), 'multipart' => false));
     if($code == 200){
       mysqli_query($conn, "INSERT INTO follows (userid) VALUES('" . $userid . "')");
       echo("Follow: " . $userid . "<br><br>");
     }
     
  } 
}

findContestTweets(array('q' => compileWinRT() . " -filter:retweets -\"free follows\" -\"must pay\" -\"tickets\" -\"bot\" -\"Zukul\" -\"bar tab\"", "count" => 10, "lang" => "en"));
findContestTweets(array('q' => "(" . compileWinRT() . ") AND tickets -filter:retweets -\"free follows\" -\"must pay\" -\"bot\"", "count" => 10, "lang" => "en", "geocode" => "41.4388246,-81.6349188,30mi"));

 $conn->close(); 
 
 ?>